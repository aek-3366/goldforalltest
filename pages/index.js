import Home from "@/components/contents/home";
import React from "react";

export default function index() {
  return (
    <div>
      <Home />
    </div>
  );
}
