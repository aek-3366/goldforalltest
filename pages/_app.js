import "@/styles/globals.css";
import "../i18n";
import Context from "../context/LanguageContextProvider";
export default function App({ Component, pageProps }) {
  return (
    <>
      <Context>
        <Component {...pageProps} />
      </Context>
    </>
  );
} 
