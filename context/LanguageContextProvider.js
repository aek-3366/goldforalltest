import { NoSsr } from "@mui/material";
import { createContext, useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

const LanguageContext = createContext();

function LanguageContextProvider({ children }) {
  const [language, setLanguage] = useState(""); //ภาษา
  const [flag, setFlag] = useState("");// ธง
  const [isVersion, setVersion] = useState(false); // setVersion เปลี่ยนเวอร์ชั่น th en

  const { i18n } = useTranslation();

  useEffect(() => {
    const getLang = localStorage.getItem("selectedLang");
    console.log("getLang", getLang);
    if (getLang === "th") {
      setFlag("../img/th.png");
      setVersion(true);
      i18n.changeLanguage(getLang);
    } else if (getLang === "en") {
      setFlag("../img/english.png");

      setVersion(false);
      i18n.changeLanguage(getLang);
    } else {
      setFlag("../img/th.png");
      setVersion(true);
      i18n.changeLanguage("th");
    }
  }, []);

  return (
    <LanguageContext.Provider
      value={{
        language,
        setLanguage,
        flag,
        setFlag,
        isVersion,
        setVersion,
      }}
    >
      <NoSsr>{children}</NoSsr>
    </LanguageContext.Provider>
  );
}

export const useChangeLanguage = () => useContext(LanguageContext);

export default LanguageContextProvider;
