import React, { useState } from "react";
import { Fragment } from "react";
import { Disclosure, Menu, Transition } from "@headlessui/react";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import logoshow from "../../public/img/logoshow.png";
import Image from "next/image";
// import { MdOutlineLanguage } from "react-icons/md";
import { useRouter } from "next/router";
import th from "../../public/img/th.png";
import English from "../../public/img/english.png";
import { useTranslation } from "react-i18next";
import { useChangeLanguage } from "@/context/LanguageContextProvider";

export default function Navbars() {
  const { setLanguage, flag, setFlag, setVersion } = useChangeLanguage();

  const { t, i18n } = useTranslation();

  const handleToggleImages = (lang) => {
    localStorage.setItem("selectedLang", lang);
    setLanguage(lang);
    if (lang == "th") {
      setFlag("../img/th.png");
      // setVersion(true); เช็คเปลี่ยนเวอร์ชั่น
      setVersion(true);
    } else {
      setFlag("../img/english.png");
      setVersion(false);
    }
    i18n.changeLanguage(lang);
  };

  const router = useRouter();
  const navigation = [
    { name: t("home"), href: "/", current: true },
    { name: t("goldsaveing"), href: "/goldsavings", current: false },
    { name: t("savices"), href: "", current: false },
    { name: t("newandactivites"), href: "/newandactivities", current: false },
    { name: t("contactus"), href: "/contactus", current: false },
  ];

  const handleNavigation = (href) => {
    router.push(href);
  };

  return (
    <div>
      <Disclosure as="nav" className="">
        {({ open }) => (
          <>
            <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
              <div className="relative flex h-16 items-center justify-between">
                <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                  <Disclosure.Button className="relative inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                    <span className="absolute -inset-0.5" />
                    <span className="sr-only">Open main menu</span>
                    {open ? (
                      <XMarkIcon
                        className="block h-6 w-6 z-10"
                        aria-hidden="true"
                      />
                    ) : (
                      <Bars3Icon
                        className="block h-6 w-6 z-10"
                        aria-hidden="true"
                      />
                    )}
                  </Disclosure.Button>
                </div>

                <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
                  <div className="flex flex-shrink-0 items-center">
                    <Image
                      className="md:w-[4rem] md:h-[5rem] lg:w-[12rem] lg:h-[12rem] lg:mt-8 xl:w-[10rem]  w-14 h-16 "
                      src={logoshow}
                      alt=""
                    />
                  </div>
                </div>

                <div className="absolute  inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                  <div className="hidden sm:ml-6 sm:block">
                    <div className="flex space-x-4  ">
                      {navigation.map((item) => (
                        <a
                          key={item.name}
                          href={item.href}
                          onClick={(e) => {
                            e.preventDefault();
                            handleNavigation(item.href);
                          }}
                          className={
                            router.pathname === item.href
                              ? "text-white rounded-full shadow-lg bg-[#d3b761] p-3 py-2 text-xs font-bold hover:bg-[#d3b761] z-20  "
                              : "text-[#fdd663] rounded-full shadow-lg bg-white p-3 py-2 text-xs font-bold hover:bg-[#d3b761] hover:text-white z-20 "
                          }
                        >
                          {item.name}
                        </a>
                      ))}
                    </div>
                  </div>
                  <Menu as="div" className="relative ml-3">
                    <div>
                      <Menu.Button className="relative flex rounded-full text-sm ">
                        <span className="absolute -inset-1.5" />
                        <span className="sr-only">Open user menu</span>
                        <img
                          className="h-8 w-8 rounded-full z-10"
                          src={flag}
                          alt=""
                        />
                      </Menu.Button>
                    </div>
                    <Transition
                      as={Fragment}
                      enter="transition ease-out duration-100"
                      enterFrom="transform opacity-0 scale-95"
                      enterTo="transform opacity-100 scale-100"
                      leave="transition ease-in duration-75"
                      leaveFrom="transform opacity-100 scale-100"
                      leaveTo="transform opacity-0 scale-95"
                    >
                      <Menu.Items className="absolute right-0 z-20 mt-2 w-28 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                        <div
                          className="grid grid-cols-3 hover:bg-slate-100 cursor-pointer"
                          onClick={() => handleToggleImages("th")}
                        >
                          <div className="col-span-1 grid justify-center">
                            <Image
                              className="h-5 w-5 rounded-full"
                              style={{ margin: "auto" }}
                              src={th}
                              alt=""
                            />
                          </div>
                          <div className="col-span-2 z-20">
                            <Menu.Item>
                              <div className="block px-4 py-2 text-sm text-gray-700 -ml-3">
                                Thailand
                              </div>
                            </Menu.Item>
                          </div>
                        </div>
                        <div
                          className="grid grid-cols-3 cursor-pointer hover:bg-slate-100"
                          onClick={() => handleToggleImages("en")}
                        >
                          <div className="col-span-1 grid justify-center">
                            <Image
                              className="h-5 w-5 rounded-full"
                              style={{ margin: "auto" }}
                              src={English}
                              alt=""
                            />
                          </div>
                          <div className="col-span-2">
                            <Menu.Item>
                              <div className="block px-4 py-2 text-sm text-gray-700 -ml-3 ">
                                English
                              </div>
                            </Menu.Item>
                          </div>
                        </div>
                      </Menu.Items>
                    </Transition>
                  </Menu>
                </div>
              </div>
            </div>
            {/* Moblie*/}
            <Disclosure.Panel className="sm:hidden absolute z-50 ">
              <div className="space-y-1 px-2 pb-3 pt-2 bg-white   shadow-md rounded-md">
                {navigation.map((item) => (
                  <Disclosure.Button
                    key={item.name}
                    as="a"
                    href={item.href}
                    className="block rounded-md px-3 py-2 text-base font-medium bg-white hover:bg-yellow-500 hover:text-white"
                    aria-current={item.current ? "page" : undefined}
                    onClick={(e) => {
                      e.preventDefault();
                      handleNavigation(item.href);
                    }}
                  >
                    {item.name}
                  </Disclosure.Button>
                ))}
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
      <div></div>
    </div>
  );
}
