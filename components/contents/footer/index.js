import React from "react";
import styles from "./footer.module.scss";
import Image from "next/image";
import iconshow from "../../../public/img/iconshow.png";
import { BsFacebook, BsFillTelephoneFill } from "react-icons/bs";
import { IoMdMail } from "react-icons/io";
import { AiFillTwitterCircle } from "react-icons/ai";
import { FaInstagramSquare, FaYoutube } from "react-icons/fa";
import qrcode from "../../../public/img/qrcode.png";
import { useTranslation } from "react-i18next";

export default function Footer() {
  const { t } = useTranslation();

  const openlinkFb = () => {
    window.open("https://www.facebook.com/GoldforallTH");
  };
  const openlinkinstagram = () => {
    window.open("https://www.instagram.com/goldforall_thailand");
  };
  const openlinkTiktok = () => {
    window.open("https://www.tiktok.com/@goldforall_th");
  };
  const openlinYouTube = () => {
    alert("Sorry, there is no information available.");
  };

  return (
    <div>
      <div className={styles.footers}>
        <div className="flex lg:flex-row lg:p-10 p-6 flex-col lg:justify-center lg:items-center gap-5">
          {/*1*/}
          <div className="items-center">
            <div className="flex gap-3">
              <div className="rounded-full shadow-md p-2 bg-white">
                <div className="lg:h-[4rem] lg:w-[4rem] h-[2rem] w-[2rem]">
                  <a href="#">
                    <Image
                      className="object-cover w-full h-full"
                      alt="img"
                      src={iconshow}
                    />
                  </a>
                </div>
              </div>

              <span
                className="lg:text-3xl xl:text-5xl text-2xl text-white font-bold items-center mt-3"
                style={{
                  textShadow: "5px 5px 5px #A78543",
                }}
              >
                {t("foldforallFooter")}
              </span>
            </div>
            <div className="lg:text-lg xl:text-xl text-lg  font-medium text-yellow-700 py-5">
              {t("headoffice")}
            </div>
            <div className="lg:text-lg text-lg font-medium text-yellow-700 mb-2">
              {t("address")}
            </div>
            <div className="lg:text-lg text-lg font-medium text-yellow-700 ">
              {t("addressData")}
            </div>
          </div>
          {/*2*/}
          <div className="flex flex-col gap-3 lg:-mt-10 ">
            <div className="flex gap-2">
              <div>
                <BsFillTelephoneFill size={25} color="white" />
              </div>
              <div className="flex flex-col lg:textlg text-lg font-medium text-yellow-700 l ">
                <div>093-663-6469</div>
              </div>
            </div>

            <div className="flex gap-2 w-full h-full">
              <div>
                <IoMdMail size={25} color="white" />
              </div>

              <div className="flex flex-col text-lg font-medium text-yellow-700 lg:text-xl">
                <div className="mb-4 w-full h-full items-center">
                  support@goldforallthailand.com
                </div>
                <div className="">{t("contact")}</div>
                <div className="flex flex-row gap-3">
                  <div>
                    <BsFacebook
                      size={30}
                      color="#385898"
                      className="cursor-pointer btnd"
                      onClick={() => openlinkFb()}
                    />
                  </div>
                  <div>
                    {/* <FaInstagramSquare size={30} color="#c58af9" /> */}
                    <img
                      src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Instagram_icon.png/2048px-Instagram_icon.png"
                      className="w-7 h-7 cursor-pointer btnd"
                      alt=""
                      onClick={() => openlinkinstagram()}
                    />
                  </div>
                  <div>
                    <FaYoutube
                      size={30}
                      color="#F02525"
                      className="cursor-pointer btnd"
                      onClick={openlinYouTube}
                    />
                  </div>

                  <div>
                    <img
                      src="https://static.vecteezy.com/system/resources/thumbnails/023/986/492/small/tiktok-logo-tiktok-logo-transparent-tiktok-icon-transparent-free-free-png.png"
                      className="w-8 h-8 cursor-pointer btnd"
                      alt=""
                      onClick={() => openlinkTiktok()}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/*3*/}
          <div className="flex items-center justify-center">
            <figure className="max-w-lg">
              <Image
                className="h-auto max-w-full rounded-lg "
                src={qrcode}
                alt=""
              />
            </figure>
          </div>
        </div>
      </div>
    </div>
  );
}
