import React from "react";
import styles from "./goldforalltext.module.scss";
import { Box } from "@mui/material";
import { useTranslation } from "react-i18next";

export default function Goldforalltext() {
  const { t } = useTranslation();
  return (
    <div>
      <div className={styles.imagecoldprice}>
        <Box className="">
          <div className="w-full h-screen flex flex-col justify-center items-center">
            <div className="xl:text-5xl  lg:text-3xl text-2xl font-semibold text-center text-[#DFBD68]">
              {t("GoldForAll")}
            </div>
            <div className="mt-6 lg:text-2xl text-xl text-[#926F34] text-center">
              <h1 className="">{t("GoldForAllserviceplatform")}</h1>
              <h1>{t("whichispriceuptodirectfromGoldAssociation")}</h1>
              <h1>{t("Throughourserviceall")}</h1>
              <h1>{t("qualitywitha_a_Partner")}</h1>
              {/* <h1>{t("save_the_gold")}</h1> */}
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
