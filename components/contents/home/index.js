import React, { useEffect, useState } from "react";
import phone from "../../../public/img/phone.png";
import styles from "./home.module.scss";
import Image from "next/image";
import { Divider } from "@mui/material";
import Navbars from "@/components/navbars";
import GraphandPrice from "../graph-an- price";
import Goldforalltext from "../goldforalltext";
import Theadvantages from "../the-advantages";
import Calculate from "../calculate";
import Newapplications from "../new-applications";
import Footer from "../footer";
import AOS from "aos";
import "aos/dist/aos.css";
import { useChangeLanguage } from "@/context/LanguageContextProvider";

export default function Home() {
  const { isVersion } = useChangeLanguage();

  useEffect(() => {
    AOS.init({
      duration: 1000,
    });
  }, []);

  return (
    <div>
      {isVersion ? (
        <div className={styles.imageth}>
          <Navbars />
          <div
            className="flex flex-col items-center justify-center w-full h-full absolute -top-7 lg:space-y-6 "
            data-aos="fade-right"
            data-aos-offset="300"
            data-aos-easing="ease-in-sine"
          >
            <div
              className="md:text-3xl lg:text-3xl xl:text-3xl 2xl:text-4xl font-bold text-[#DFBD69] normal-case  "
              style={{ textShadow: "5px 5px 5px white" }}
            >
              กำหนดความมั่งคั่งของคุณด้วยปลาย
            </div>
            <div className="md:text-4xl lg:text-5xl xl:text-5xl 2xl:text-6xl font-thin text-[#DFBD69]">
              "โกลด์ ฟอร์ ออล"
            </div>
          </div>
        </div>
      ) : (
        <div className={styles.imagecontainer}>
          <Navbars />
          <div className="grid lg:grid-cols-2 lg:mt-36 xl:mt-0">
            <div className="grid justify-center items-center p-10  ">
              <div className="flex flex-col space-y-[3rem]">
                <div
                  className="lg:text-4xl md:text-xl font-bold  text-yellow-700 text-center"
                  data-aos="fade-right"
                  data-aos-easing="ease-out-cubic"
                  data-aos-duration="3000"
                >
                  DESTINE YOUR OWN WEALTH
                  <Divider />
                </div>
                <div>
                  <center>
                    <button className="bg-gradient-to-r from-[#ae8625] to-[#edc967]  hover:bg-gradient-to-r hover:from-[#ae8625] hover:to-[#F7EF8A]   p-3 sm:text-sm md:w-[13rem] md:text-lg lg:w-[17rem] lg:text-xl  xl:text-2xl xl:w-[20rem] rounded-full shadow-xl  text-white hover:bg-yellow-700 btnd">
                      JUST CLICK
                    </button>
                  </center>
                </div>
                <div className="text-center text-xl md:text-4xl lg:text-5xl xl:text-6xl font-medium text-yellow-700 ">
                  <h5>Gold For All</h5>
                </div>
              </div>
            </div>

            <div className="w-full h-full ">
              <div className="w-full relative pt-[100%]">
                <Image
                  data-aos="fade-up"
                  data-aos-anchor-placement="top-center"
                  src={phone}
                  alt="profile"
                  objectFit="cover"
                  fill
                  className="w-full h-full top-0 left-0 object-cover 0"
                />
              </div>
            </div>
          </div>
        </div>
      )}

      <div>
        <GraphandPrice />
      </div>

      <div>
        <Goldforalltext />
      </div>
      <div>
        <Theadvantages />
      </div>
      <div>
        <Calculate />
      </div>
      <div>
        <Newapplications />
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}
