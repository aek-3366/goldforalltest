import Navbars from "@/components/navbars";
import React from "react";
import styles from "./new-and-activites.module.scss";
import newapp from "../../../public/img/newapp.png";
import ted from "../../../public/img/ted.jpg";
import pipi from "../../../public/img/pipi.jpg";
import tttt from "../../../public/img/tttt.jpg";
import Image from "next/image";
import { Box } from "@mui/material";
// import AOS from "aos";
import "aos/dist/aos.css";
import { useTranslation } from "react-i18next";

export default function Activities() {
  const { t } = useTranslation();
  return (
    <div>
      <div className={styles.imagecoldprice}>
        <Navbars />
        <Box>
          <div className="md:py-6 lg:py-[4rem] py-4 ">
            <h1 className="lg:text-5xl text-xl font-semibold text-center text-[#926F34]">
              {t("newsand")}
            </h1>
          </div>

          <div className=" w-full h-full relative pt-[40%] md:-mt-[3rem] lg:-mt-[12rem] hidden md:block">
            <Image
              // data-aos="fade-up"
              // data-aos-anchor-placement="center-center"
              src={newapp}
              alt="profile"
              objectFit="cover"
              fill
              className="z-10 w-full h-full xl:max-w-max lg:max-w-max  lg:ml-[6rem] xl:ml-[11rem]  xl:max-h-full 2xl top-0 left-0 object-cover absolute"
            />
          </div>

          <div className="space-y-8">
            <div className="md:hidden px-[40px]">
              <div className="max-w-lg  shadow-md rounded-2xl bg-white p-3 ">
                <Image
                  className=" max-w-full min-h-full  object-cover"
                  src={pipi}
                  alt=""
                />
              </div>
            </div>

            <div className="md:hidden px-[40px]">
              <div className="max-w-lg  shadow-md rounded-2xl bg-white p-3 ">
                <Image
                  className=" max-w-full min-h-full  object-cover"
                  src={ted}
                  alt=""
                />
              </div>
            </div>

            <div className="md:hidden px-[40px]">
              <div className="max-w-lg  shadow-md rounded-2xl bg-white p-3 ">
                <Image
                  className=" max-w-full min-h-full  object-cover"
                  src={tttt}
                  alt=""
                />
              </div>
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
