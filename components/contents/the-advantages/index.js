import React from "react";
import styles from "./theadvantages.module.scss";
import { Box } from "@mui/material";
import Image from "next/image";
import staring from "../../../public/img/staring.png";
import mobileth from "../../../public/img/mobileth.png";
import labtopth from "../../../public/img/labtopth.png";
import doccc from "../../../public/img/doccc.png";
import { useTranslation } from "react-i18next";

export default function Theadvantages() {
  const { t } = useTranslation();

  return (
    <div className={styles.imagecoldprice}>
      <Box>
        <div className="md:py-6 lg:py-[4rem] py-4 ">
          <h1 className="lg:text-5xl text-xl font-semibold text-center text-[#926F34]">
            {t("the_advantages")}
          </h1>
          <div className="lg:mt-6 text-xl font-medium  lg:text-3xl  text-[#926F34] text-center">
            <h1 className="">{t("ofsavinggolGoldForall")}</h1>
          </div>
        </div>

        <div className="w-full h-full relative pt-[50%] md:-mt-[3rem] lg:-mt-[12rem] hidden md:block">
          <Image
            src={staring}
            alt="profile"
            objectFit="cover"
            fill
            className="w-full h-full xl:max-w-max lg:max-w-max xl:ml-[20rem] lg:ml-[14rem] 2xl:ml-[20rem] xl:max-h-full 2xl top-0 left-0 object-cover absolute"
            data-aos="zoom-in-down"
          />
        </div>
        {/*mobileth*/}
        <div className="max-w-lg md:hidden ml-10 -mt-[10rem] ">
          <Image
            className="h-[40rem]  rounded-lg  object-cover"
            objectFit="cover"
            src={mobileth}
            alt=""
          />
        </div>

        <div className="max-w-lg md:hidden -mt-[15rem]  mr-4">
          <Image
            className="h-[35rem]  rounded-lg  object-cover"
            src={labtopth}
            alt=""
          />
        </div>
        <div>
          <Image
            className="h-[40rem] max-w-full -mt-[12rem] object-cover md:hidden"
            src={doccc}
            alt="image description"
          />
        </div>
      </Box>
    </div>
  );
}
