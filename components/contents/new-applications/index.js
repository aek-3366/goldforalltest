import React from "react";
import styles from "./applications.module.scss";
import phonedata from "../../../public/img/mobile9.png";
import Image from "next/image";
import { AiFillApple } from "react-icons/ai";
import play from "../../../public/img/play.png";
import { Avatar } from "@mui/material";
import { useTranslation } from "react-i18next";
export default function Newapplications() {
  const { t } = useTranslation();

  const download = () => {
    alert("Download and apply now.");
  };

  return (
    <div>
      <div className={styles.calculate}>
        <div className="grid lg:grid-cols-2 md:grid-cols-2 p-10">
          <div className="flex flex-col w-full h-full ">
            <div className="lg:text-3xl xl:text-4xl 2xl:text-4xl md:text-xl font-bold lg:px-[4rem]  text-yellow-700 text-left mb-2 lg:mt-12 mt-0">
              {t("New_applications_All")}
            </div>
            <div className="lg:mb-[3rem] mb-5">
              <div className="lg:text-2xl md:text-xl  lg:px-[4rem]  font-medium  text-black/75 text-left">
                {t("Developed_and_convenience")}
              </div>
            </div>

            <div className="text-left text-base md:text-xl  font-medium text-yellow-700 lg:px-[4rem] flex flex-row gap-2 mb-2">
              {t("anytime_anywhere")}
            </div>

            <div className="text-left text-base md:text-xl font-medium text-yellow-700 lg:px-[4rem]  gap-2 mb-2">
              {t("Ready_wish_according")}
            </div>
            <div className="flex lg:flex-row flex-col gap-5 w-full h-full ">
              <div className="flex-col lg:px-[4rem] lg:mt-12 mt-4 space-y-3">
                <div className="lg:text-lg xl:text-xl font-medium mb-2">
                  {t("ios_and_android")}
                </div>

                <div
                  className="bg-gradient-to-r from-[#A78543] to-yellow-600 shadow-md rounded-full p-3 text-center text-white mb-2
                   lg:text-sm md:text-sm font-medium cursor-pointer hover:bg-gradient-to-r hover:from-[#A78543] hover:to-yellow-400 "
                  style={{
                    textShadow: "5px 5px 5px #A78543",
                  }}
                  onClick={() => download()}
                >
                  {t("Download_and")}
                </div>

                <div className="flex lg:flex-col xl:flex-row flex-row gap-3 w-full ">
                  <div className="bg-black w-full text-white p-1 text-center rounded-md shadow-md cursor-pointer hover:bg-black/80 ">
                    <div className="grid grid-cols-4 ">
                      <div className="col-span-1">
                        <AiFillApple className="mt-1" size={27} color="white" />
                      </div>
                      <div className="col-span-3">
                        <div className="xl:text-[10px] text-xs md:text-xs text-left">
                          Dowload on the
                        </div>
                        <div className="text-left  xl:text-sm text-base">
                          App Store
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="bg-black w-full text-white p-1 text-center rounded-md shadow-md cursor-pointer hover:bg-black/80">
                    <div className="grid grid-cols-4 xl:mt-2 mt-1 2xl:mt-0 md:mt-1">
                      <div className="col-span-1">
                        <Image className="mt-2.5 h-5 w-5" src={play} />
                      </div>
                      <div className="col-span-3 xl:mt-1 mt-0">
                        <div className="text-xs text-left">Get it On</div>
                        <div className="text-left xl:text-sm text-base">
                          App Store
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="lg:mt-12 mt-4 flex-col space-y-7">
                <div className="rounded-l-full p-3 text-[#A78543] w-full bg-white text-2xl md:ml-10 ml-0  ">
                  <div className="flex gap-2 px-4 justify-center items-center">
                    <Avatar className="font-bold bg-gradient-to-r from-[#A78543] to-yellow-600  justify-center items-center text-white">
                      1
                    </Avatar>
                    <div className="lg:text-base xl:text-xl font-medium text-base ">
                      {t("Gold_price_alert_else")}
                    </div>
                  </div>
                </div>

                <div className="rounded-l-full p-3 text-[#A78543] w-full bg-white text-2xl  ml-0  ">
                  <div className="flex gap-2 px-4 justify-center items-center">
                    <Avatar className="font-bold bg-gradient-to-r from-[#A78543] to-yellow-600  justify-center items-center text-white">
                      2
                    </Avatar>
                    <div className="lg:text-base xl:text-xl font-medium text-base ">
                      {t("buy_sell")}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="flex flex-row"></div>
          </div>

          <div className="w-full h-full ">
            <div className="w-full relative pt-[70%]">
              <Image
                src={phonedata}
                alt="profile"
                objectFit="cover"
                fill
                className="w-full h-full top-0 left-0 object-cover 0"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
