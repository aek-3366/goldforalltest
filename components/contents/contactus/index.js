import React, { useState } from "react";
import styles from "./graph.module.scss";
import Navbars from "@/components/navbars";
import { BiPhone } from "react-icons/bi";
import { AiOutlineMail } from "react-icons/ai";
import swal from "sweetalert2";
import emailjs from "@emailjs/browser";
import { useTranslation } from "react-i18next";
import SendIcon from "@mui/icons-material/Send";
export default function Contactusdata() {
  const { t } = useTranslation();

  const [values, setValues] = useState({
    firstName: "",
    email: "",
    phone: "",
    line: "",
    detail: "",
  });

  const handleOnChangeValues = (e) => {
    const { name, value } = e.target;
    setValues((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const sendEmail = (e) => {
    e.preventDefault();
    if (
      !values.firstName ||
      !values.phone ||
      // !values.email ||
      // !values.line ||
      !values.detail
    ) {
      swal.fire({
        icon: "error",
        title: "กรุณากรอกข้อมูลให้ครบ",
        text: "Please fill in all fields",
      });
      return;
    }

    const valuesdata = `name: ${values.firstName} \n phone: ${values.phone} \n email: ${values.email} \n line: ${values.line} \n detail: ${values.detail} \n`;
    console.log(values, "value");

    emailjs
      .send(
        "service_ocafu19",
        "template_th99vrq",
        {
          from_name: values.firstName,
          email_from: values.email,
          message: valuesdata,
          reply_to: values.email,
        },
        "7ksSo5sXvUoAZq-Dm"
      )
      .then(
        (result) => {
          if (result.status === 200 && result.text === "OK") {
            swal.fire(
              "ขอบคุณสำหรับข้อมูล",
              "ทางเราจะติดต่อกลับลูกค้าโดยเร็ว",
              "success"
            );
            resetForm();
          } else {
            console.log("error");
          }
        },
        (error) => {
          console.log(error);
        }
      );
  };

  const resetForm = () => {
    setValues({
      firstName: "",
      phone: "",
      email: "",
      line: "",
      detail: "",
    });
  };

  return (
    <div>
      <div className={styles.imagecoldprice}>
        <Navbars />
        <div>
          <center>
            <div className="mt-10  mb-4">
              <div
                className="flex justify-center items-center lg:text-xl  font-medium bg-gradient-to-r from-yellow-500 to-yellow-600 
              hover:from-yellow-500 hover:to-yellow-500 cursor-pointer rounded-full  p-3 h-full w-[10rem] shadow-md text-white"
              >
                {t("Contact_us")}
              </div>
            </div>
            <div></div>
          </center>
          <h1 className="md:px-[5rem] lg:px-[10rem] px-4 mb-2">
            {t("Contact")}
          </h1>

          {/* <div className="grid md:grid-cols-5 lg:grid-cols-5 xl:grid-cols-5 2xl::grid-cols-5 md:px-[2rem] lg:px-[10rem] gap-8 px-4"> */}
          <div className="flex lg:flex-row flex-col gap-8 px-4 lg:px-[10rem]">
            <form className="lg:w-5/6 w-full" onSubmit={(e) => sendEmail(e)}>
              <div className="flex gap-[1rem] mb-2">
                <input
                  value={values.firstName}
                  name="firstName"
                  onChange={(e) => handleOnChangeValues(e)}
                  type="text"
                  className="w-full h-9 rounded-md outline-none  px-2 text-sm"
                  placeholder="ชื่อ นามสกุล *"
                />
                <input
                  value={values.phone}
                  name="phone"
                  onChange={(e) => handleOnChangeValues(e)}
                  type="number"
                  className="w-full h-9 rounded-md outline-none  px-2 text-sm"
                  placeholder="เบอร์โทรศัพท์"
                />
              </div>
              <div className="flex gap-[1rem] mb-2">
                <input
                  type="text"
                  className="w-full h-9 rounded-md outline-none px-2 text-sm"
                  placeholder="อีเมล (ถ้ามี)"
                  value={values.email}
                  name="email"
                  onChange={(e) => handleOnChangeValues(e)}
                />
                <input
                  value={values.line}
                  name="line"
                  onChange={(e) => handleOnChangeValues(e)}
                  type="text"
                  className="w-full h-9 rounded-md outline-none  px-2 text-sm"
                  placeholder="LINE (ถ้ามี)"
                />
              </div>
              <textarea
                onChange={(e) => handleOnChangeValues(e)}
                value={values.detail}
                name="detail"
                id="w3review"
                rows="5"
                className="w-full outline-none p-2 mb-4"
                placeholder="ข้อความ"
              ></textarea>
              <div className="flex gap-3 ">
                <div
                  className="font-medium bg-gradient-to-r from-yellow-500 to-yellow-600 space-x-1 
              hover:from-yellow-500 hover:to-yellow-500 cursor-pointerounded-full p-2 shadow-md rounded-md text-white text-base cursor-pointer
              btnd"
                >
                  <SendIcon className="" />
                  <button className="" type="submit">
                    {t("message")}
                  </button>
                </div>
              </div>
            </form>

            <div className="lg:w-1/6 w-full h-full">
              <div className="flex justify-center">
                <div className="w-fit rounded-full font-medium shadow-md bg-white text-black p-1 px-3 text-center mb-2">
                  {t("communication")}
                </div>
              </div>
              <div className="flex gap-2">
                <BiPhone className="mt-1 " />
                <h1 className="font-medium">Telephoe</h1>
              </div>
              <h1 className="mx-[24px] font-medium mb-4">093-663-6469</h1>

              <div className="flex gap-2">
                <AiOutlineMail className="mt-1 " />
                <h1 className="font-medium">Email</h1>
              </div>
              <h1 className="mx-[24px] font-medium">
                support@goldforallthailand.com
              </h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
