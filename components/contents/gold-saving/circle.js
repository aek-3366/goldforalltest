import React from "react";
import styles from "./graph.module.scss";
import goldforalldata from "../../../public/Images/goldforalldata.png";
import backthth from "../../../public/Images/backthth.png";

import singlering from "../../../public/Images/singlering.png";
import checkin from "../../../public/img/checkin.png";
import dead from "../../../public/Images/dead.png";
import havean from "../../../public/Images/havean.png";
import code from "../../../public/Images/code.png";
import aboxofvans from "../../../public/Images/aboxofvans.png";
import savinggold from "../../../public/Images/savinggold.png";
import Image from "next/image";
import { useChangeLanguage } from "@/context/LanguageContextProvider";
import { useTranslation } from "react-i18next";

export default function Circle() {
  const { isVersion } = useChangeLanguage();
  const { t } = useTranslation();

  return (
    <div>
      {isVersion ? (
        <div className={styles.backdata}>
          <div className="flex-col gap-3 absolute hidden md:block py-6 lg:mt-7 xl:mt-7 2xl:mt-24 ">
            <div
              className=" justify-center items-center w-full  h-full lg:text-4xl  md:text-2xl  text-xl font-semibold  text-[#DFBD69] leading-none uppercase text-center "
              style={{
                textShadow: "5px 5px 5px white",
              }}
            >
              วิธีการออมทอง
            </div>

            <div
              className=" justify-center items-center w-full h-full relative lg:text-4xl  md:text-2xl font-semibold  text-[#F7EF8A] leading-none uppercase text-center"
              style={{
                textShadow: "5px 5px 5px #A78543",
              }}
            >
              ออมเพื่ออนาคตกับ โกลด์ ฟอร์ ออล
            </div>
          </div>

          <div className="hidden md:block relative">
            <Image
              className="h-auto w-full object-contain"
              src={backthth}
              alt="image description"
            />
          </div>
          {/*Moblie */}
          <div className="md:hidden ">
            <div className="flex flex-col items-center p-3">
              <div
                className="text-[#DFBD69] text-xl font-bold"
                style={{
                  textShadow: "5px 5px 5px white",
                }}
              >
                {t("How_to_savegold")}
              </div>
              <div className="text-[#DFBD69] text-sm font-bold">
                {t("savefor_the_future")}
              </div>
            </div>

            <div className="grid grid-cols-12 gap-3 p-3">
              <div className="col-span-4 items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={savinggold}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center">{t("saving_gold")}</div>
                <div className=" text-[#DFBD69] text-center text-xs">
                  {t("Pay_out_receive")}
                </div>
              </div>

              <div className="col-span-4  items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={dead}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center">{t("have_ID_card")}</div>
                <div className=" text-[#DFBD69] text-center">
                  {t("can_open_saavings")}
                </div>
                <div className=" text-red-600 text-center text-xs">
                  {t("can_open_saavings")}
                </div>
              </div>

              <div className="col-span-4 items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={checkin}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center">{t("start")}</div>
                <div className=" text-[#DFBD69] text-center text-xs">
                  {t("save_only_baht")}
                </div>
              </div>
            </div>

            <div className="grid grid-cols-12 gap-3 p-3">
              <div className="col-span-4 items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={havean}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center">
                  {t("Have_application")}
                </div>
                <div className=" text-[#DFBD69] text-center text-xs">
                  {t("to_make_safe")}
                </div>
              </div>

              <div className="col-span-4  items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={code}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center">
                  {t("with_gold_saving_code")}
                </div>
                <div className=" text-[#DFBD69] text-center">
                  {t("and_gold_saving_every_time")}
                </div>
              </div>

              <div className="col-span-4 items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={aboxofvans}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center">
                  {t("custommers_can")}
                </div>
                <div className=" text-[#DFBD69] text-center text-xs">
                  {t("get_gold_shop")}
                </div>
              </div>
            </div>
            <div className="">
              <div className="grid justify-center items-center">
                <Image
                  className="h-24 w-28 object-contain"
                  src={singlering}
                  alt=""
                />
              </div>
              <div className="font-bold text-center">{t("gold_price")}</div>
              <div className=" text-[#DFBD69] text-center text-xs mb-5">
                {t("day_of_payment")}
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className={styles.backdata}>
          <div className="flex-col gap-3 absolute hidden md:block py-6 lg:mt-7 xl:mt-7 2xl:mt-24 ">
            <div
              className=" justify-center items-center w-full  h-full lg:text-2xl  md:text-2xl  text-xl font-semibold  text-[#DFBD69] leading-none uppercase text-center "
              style={{
                textShadow: "5px 5px 5px white",
              }}
            >
              How to save gold
            </div>
            <div
              className=" justify-center items-center w-full h-full relative lg:text-2xl  md:text-2xl font-semibold  text-white leading-none uppercase text-center"
              style={{
                textShadow: "5px 5px 5px #A78543",
              }}
            >
              Save for the Future with Gol For All
            </div>
          </div>

          <div className="hidden md:block relative">
            <Image
              className="h-auto w-full object-contain"
              src={goldforalldata}
              alt="image description"
            />
          </div>
          {/*Moblie */}
          <div className="md:hidden ">
            <div className="flex flex-col items-center p-3">
              <div
                className="text-[#DFBD69] text-xl font-bold"
                style={{
                  textShadow: "5px 5px 5px white",
                }}
              >
                How to save gold
              </div>
              <div className="text-[#DFBD69] text-sm font-bold">
                save for the Future with Gold for All
              </div>
            </div>

            <div className="grid grid-cols-12 gap-3 p-3">
              <div className="col-span-4 items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={savinggold}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center">Saving gold</div>
                <div className=" text-[#DFBD69] text-center text-xs">
                  Pay out receive
                </div>
              </div>

              <div className="col-span-4  items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={dead}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center"> have ID card</div>
                <div className=" text-[#DFBD69] text-center">
                  can open a gold saavings
                </div>
                <div className=" text-red-600 text-center text-xs">
                  *To cancel gold saving
                </div>
              </div>

              <div className="col-span-4 items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={checkin}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center">Saving gold</div>
                <div className=" text-[#DFBD69] text-center text-xs">
                  Pay out receive
                </div>
              </div>
            </div>

            <div className="grid grid-cols-12 gap-3 p-3">
              <div className="col-span-4 items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={havean}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center">Have an application</div>
                <div className=" text-[#DFBD69] text-center text-xs">
                  to make a list easy and safe.
                </div>
              </div>

              <div className="col-span-4  items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={code}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center">
                  with gold saving code
                </div>
                <div className=" text-[#DFBD69] text-center">
                  and gold saving certificates as evidence every time
                </div>
              </div>

              <div className="col-span-4 items-center justify-center ">
                <div className="grid items-center justify-center">
                  <Image
                    className="h-24 w-28 object-contain"
                    src={aboxofvans}
                    alt=""
                  />
                </div>
                <div className="font-bold text-center">custommers can</div>
                <div className=" text-[#DFBD69] text-center text-xs">
                  get gold at the shop
                </div>
              </div>
            </div>
            <div className="">
              <div className="grid justify-center items-center">
                <Image
                  className="h-24 w-28 object-contain"
                  src={singlering}
                  alt=""
                />
              </div>
              <div className="font-bold text-center">Gold price</div>
              <div className=" text-[#DFBD69] text-center text-xs mb-5">
                on the last day of payment (According to the gold price onthe
                closing data)
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
