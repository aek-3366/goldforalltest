import React, { useEffect } from "react";
import styles from "./graph.module.scss";
import Navbars from "@/components/navbars";
import doc from "../../../public/img/doc.png";
import Image from "next/image";
import AOS from "aos";
import "aos/dist/aos.css";
import Circle from "./circle";
import { useTranslation } from "react-i18next";

export default function GoldSaving() {
  const { t } = useTranslation();
  useEffect(() => {
    AOS.init({
      duration: 1000,
    });
  }, []);

  return (
    <div>
      <div className={styles.imagecoldprice}>
        <Navbars />
        <div className="flex justify-center md:flex-row flex-col items-center w-full md:pt-40 lg:pt-20 xl:pt-0">
          <div className="w-full h-full ">
            <div className="w-full relative pt-[100%]">
              <Image
                data-aos="zoom-in"
                src={doc}
                alt="profile"
                objectFit="cover"
                fill
                className="lg:w-11  w-full h-full max-w-2xl  flex justify-center object-cover -mt-12 md:-mt-0  xl:mt-0 "
              />
            </div>
          </div>

          <div className="flex justify-center items-center w-full h-full ">
            <div className="flex flex-col space-y-5 -m-9 md:-m-0">
              <div
                className="lg:text-6xl xl:text-7xl md:text-2xl font-medium  text-white text-2xl  text-center "
                style={{ textShadow: "5px 5px 5px #926F34" }}
                data-aos="zoom-in-down"
              >
                {t("why")}
              </div>
              <div>
                <center>
                  <div
                    className="lg:text-4xl md:text-2xl font-medium text-2xl text-white  text-center"
                    style={{ textShadow: "5px 5px 5px #926F34" }}
                  >
                    {t("goldforallgoldsaving")}
                  </div>
                </center>
              </div>
              <div className=" font-medium text-yellow-700 space-y-5 text-center">
                <div
                  className="bg-gradient-to-r from-yellow-500 to-yellow-600 selection:text-center rounded-full shadow-xl mx-10 p-4 lg:text-xl text-white hover:bg-gradient-to-r 
                hover:from-yellow-400 hover:to-yellow-500 cursor-pointer "
                >
                  {t("applyforapproval")}
                </div>
                <div
                  className="bg-gradient-to-r from-yellow-600 to-yellow-500 selection:text-center rounded-full shadow-xl mx-10 p-4 lg:text-xl text-white 
                hover:from-yellow-400 hover:to-yellow-500 cursor-pointer"
                >
                  {t("onlineshopping")}
                </div>
                <div
                  className="bg-gradient-to-r from-yellow-500 to-yellow-600 selection:text-center rounded-full shadow-xl mx-10 p-4 lg:text-xl text-white 
                hover:from-yellow-400 hover:to-yellow-500 cursor-pointer"
                >
                  {t("haveinvestment")}
                </div>
                <div
                  className="bg-gradient-to-r from-yellow-600 to-yellow-400 selection:text-center rounded-full shadow-xl mx-10 p-4 lg:text-xl text-white 
                hover:from-yellow-400 hover:to-yellow-500 cursor-pointer"
                >
                  {t("sellsavings")}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Circle />
    </div>
  );
}
