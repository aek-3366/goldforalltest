import React, { useState } from "react";
import styles from "./calculate.module.scss";
import { AiOutlineSwap } from "react-icons/ai";
import { FaEquals } from "react-icons/fa";
import { useTranslation } from "react-i18next";

export default function Calculate() {
  const [input1, setInput1] = useState("");
  const [input2, setInput2] = useState("");
  const [sum, setSum] = useState("");
  const { t } = useTranslation();

  const calculateModifiedSum = (input1Value, input2Value) => {
    const parsedInput1 = parseFloat(input1Value);
    const parsedInput2 = parseFloat(input2Value);

    if (!isNaN(parsedInput1) && !isNaN(parsedInput2)) {
      if (parsedInput2 < 15.2) {
        return (parsedInput1 + parsedInput2).toFixed(2);
      } else {
        const positiveFunction = (x) => Math.exp(x);
        return (
          parsedInput2 *
          (positiveFunction(parsedInput1) - parsedInput2)
        ).toFixed(2);
      }
    }
    return "";
  };

  const handleInput1Change = (e) => {
    setInput1(e.target.value);
    const modifiedSum = calculateModifiedSum(e.target.value, input2);
    setSum(modifiedSum);
  };

  const handleInput2Change = (e) => {
    setInput2(e.target.value);
    const modifiedSum = calculateModifiedSum(input1, e.target.value);
    setSum(modifiedSum);
  };

  return (
    <div>
      <div className={styles.imagecontainer}>
        <div className="hidden md:block">
          <div className="w-full h-full flex flex-col justify-center items-center lg:p-12 p-5 ">
            <span
              className="text-4xl font-medium text-white"
              style={{ textShadow: "5px 5px 5px #A78543" }}
            >
              {t("calculate")}
            </span>
            <span
              className="lg:mb-4 lg:text-5xl text-white font-medium"
              style={{
                textShadow: "5px 5px 5px #A78543",
              }}
            >
              {t("theprice_of_gold")}
            </span>
            <h1
              className="lg:text-xl font-medium text-[#A78543]"
              style={{
                textShadow: "5px 5px 5px white",
              }}
            >
              {t("Easily_calculate_yourself")}
            </h1>
            <h1 className="font-medium mb-10 text-[#916C30]">
              {t("Its_a_goldincluded")}
            </h1>

            <div className="flex lg:flex-row flex-col justify-between lg:space-x-7  gap-2 items-center lg:ml-0 -ml-6 ">
              <div className="ml-7 flex-col">
                <input
                  type="number"
                  value={input1}
                  onChange={handleInput1Change}
                  className="lg:w-52 lg:h-16 h-12  w-[20rem]  rounded-full shadow-md bg-white px-2  outline-none text-center"
                />
                <div
                  className="hidden md:block text-center text-white font-medium text-lg "
                  style={{ textShadow: "5px 5px 5px #A78543" }}
                >
                  {t("price_of_goldinput")}
                </div>
              </div>
              <AiOutlineSwap
                size={25}
                color="#A78543"
                className=" cursor-pointer "
              />
              <div className="ml-7 flex-col">
                <input
                  type="number"
                  value={input2}
                  onChange={handleInput2Change}
                  className="lg:w-52 lg:h-16  h-12  w-[20rem]  rounded-full shadow-md bg-white px-2  outline-none text-center"
                />
                <div
                  className=" hidden md:block text-center font-medium lg:text-lg text-white "
                  style={{ textShadow: "5px 5px 5px #A78543" }}
                >
                  {t("weight_of_gold")}
                </div>
              </div>
              <FaEquals
                size={25}
                color="#A78543"
                className=" cursor-pointer "
              />
              <div className="ml-7 flex-col">
                <input
                  readOnly
                  disabled
                  value={sum}
                  type="number"
                  className="lg:w-52 lg:h-16  w-[20rem] h-12 rounded-full shadow-md bg-white px-2  outline-none border-yellow-600 border-b-2 text-center"
                />
                <div
                  className="hidden md:block  text-center text-white font-medium lg:text-lg"
                  style={{ textShadow: "5px 5px 5px #A78543" }}
                >
                  {t("sum")}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="md:hidden w-full h-full flex flex-col justify-center items-center p-5">
          <div className="flex flex-col gap-5 text-center">
            <span
              className="text-xl font-medium text-white"
              style={{ textShadow: "5px 5px 5px #A78543" }}
            >
              CALCULATE
            </span>

            <h1
              className="text-xl font-medium text-[#A78543]"
              style={{
                textShadow: "5px 5px 5px white",
              }}
            >
              know the pice of gold by youself easily with a smart program.
            </h1>

            <h1 className="font-medium mb-10 text-[#916C30]">
              {"(it's a gold calculation. gratuity not included)"}
            </h1>
          </div>

          <div className="grid grid-cols-4 justify-center items-center mb-4">
            <div className="col-span-1">
              <p className="text-center">Gold welight</p>
            </div>
            <div className="col-span-3">
              <input
                value={input1}
                onChange={handleInput1Change}
                type="number"
                className=" h-12 w-full rounded-full shadow-md bg-white px-2  outline-none text-center "
              />
            </div>
          </div>
          <AiOutlineSwap
            size={25}
            color="#A78543"
            className=" cursor-pointer "
          />
          <div className="grid grid-cols-4 justify-center items-center mb-4">
            <div className="col-span-1">
              <p className="text-center">Cureent gold price</p>
            </div>
            <div className="col-span-3">
              <input
                value={input2}
                onChange={handleInput2Change}
                type="number"
                className=" h-12 w-full rounded-full shadow-md bg-white px-2  outline-none text-center"
              />
            </div>
          </div>
          <FaEquals size={25} color="#A78543" className=" cursor-pointer " />

          <div className="grid grid-cols-4 justify-center items-center ">
            <div className="col-span-1">
              <p className="text-center">Cureent gold price</p>
            </div>
            <div className="col-span-3">
              <input
                readOnly
                disabled
                value={sum}
                type="number"
                className=" h-12 w-full rounded-full shadow-md bg-white px-2  outline-none text-center"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
