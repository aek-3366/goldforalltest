import React, { useEffect, useRef, useState } from "react";
import styles from "./graph.module.scss";

// import moment from "moment";
// import "moment/locale/th";

import moment from "moment";
import "moment-timezone";

import "aos/dist/aos.css";
import { useTranslation } from "react-i18next";

let tvScriptLoadingPromise;

export default function GraphandPrice() {
  const onLoadScriptRef = useRef();
  const { t } = useTranslation();

  const goldforAlldata = [
    {
      name: "Update",
      bid: "1692610442",
      ask: "16:34",
      diff: 0,
    },
    {
      name: "GoldSpot",
      bid: "1889.30",
      ask: 1890.3,
      diff: "-0.7",
    },
    {
      name: "Silver",
      bid: "22.95",
      ask: 23.050000000000001,
      diff: "+0.09",
    },
    {
      name: "THB",
      bid: "35.380001",
      ask: "35.380001",
      diff: "-0.1",
    },
    {
      name: "สมาคมฯ",
      bid: "31500",
      ask: "31600",
      diff: -150,
    },
    {
      name: "96.5%",
      bid: 31610,
      ask: 31627,
      diff: "+27",
    },
    {
      name: "99.99%",
      bid: 32760,
      ask: 32777,
      diff: "",
    },
    {
      name: "GFQ23",
      bid: 31780,
      ask: 31790,
      diff: "",
    },
    {
      name: "GFV23",
      bid: 31860,
      ask: 31870,
      diff: "",
    },
    {
      name: "GFZ23",
      bid: 31930,
      ask: 31950,
      diff: "",
    },
    {
      name: "SVQ23",
      bid: 811,
      ask: 815,
      diff: "",
    },
    {
      name: "SVV23",
      bid: 813,
      ask: 817,
      diff: "",
    },
    {
      name: "SVZ23",
      bid: 815,
      ask: 819,
      diff: "",
    },
    {
      name: "GFPTM23-curr",
      bid: "",
      ask: 0,
      diff: 0,
    },
    {
      name: "GF10M23-curr",
      bid: "",
      ask: 32720,
      diff: 370,
    },
    {
      name: "GF10Q23-curr",
      bid: "",
      ask: 32800,
      diff: 360,
    },
    {
      name: "GF10V23-curr",
      bid: "",
      ask: 32840,
      diff: 310,
    },
    {
      name: "GFM23-curr",
      bid: "",
      ask: 32670,
      diff: 390,
    },
    {
      name: "GFQ23-curr",
      bid: "",
      ask: 32750,
      diff: 280,
    },
    {
      name: "GFV23-curr",
      bid: "",
      ask: 32810,
      diff: 330,
    },
    {
      name: "SVFM23-curr",
      bid: "",
      ask: 25,
      diff: 0,
    },
    {
      name: "SVFU23-curr",
      bid: "",
      ask: 25,
      diff: 0,
    },
  ];

  useEffect(() => {
    onLoadScriptRef.current = createWidget;
    if (!tvScriptLoadingPromise) {
      tvScriptLoadingPromise = new Promise((resolve) => {
        const script = document.createElement("script");
        script.id = "tradingview-widget-loading-script";
        script.src = "https://s3.tradingview.com/tv.js";
        script.type = "text/javascript";
        script.onload = resolve;

        document.head.appendChild(script);
      });
    }

    tvScriptLoadingPromise.then(
      () => onLoadScriptRef.current && onLoadScriptRef.current()
    );

    return () => (onLoadScriptRef.current = null);

    function createWidget() {
      if (
        document.getElementById("tradingview_9033b") &&
        "TradingView" in window
      ) {
        new window.TradingView.widget({
          width: "100%",
          height: 384,
          symbol: "TVC:GOLD",
          interval: "D",
          timezone: "Asia/Bangkok",
          theme: "dark",
          style: "1",
          locale: "th_TH",
          enable_publishing: false,
          allow_symbol_change: true,
          container_id: "tradingview_9033b",
        });
      }
    }
  }, []);

  let datetime = moment(new Date()).format("DD MMMM YYYY");
  const [formattedTime, setFormattedTime] = useState("");

  useEffect(() => {
    const interval = setInterval(() => {
      const currentTime = moment().tz("Asia/Bangkok").format("h:mm:ss");
      setFormattedTime(currentTime);
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  // console.log("formattedTime",formattedTime)

  return (
    <div>
      <div className={styles.imagecoldprice}>
        <div className="w-full h-screen flex justify-center items-center  relative">
          <div className="flex lg:flex-row flex-col gap-8 absolute top-[5rem] px-4 md:px-[5rem] w-full">
            <div
              className="bg-white border rounded-none w-full h-full p-2"
              data-aos="flip-right"
              data-aos-easing="ease-out-cubic"
              data-aos-duration="3000"
            >
              <div className="flex justify-center items-center">
                <div className="-top-8 cursor-pointer bg-gradient-to-r from-[#F7eF8A] text-white to-[#D2AC47] text-base p-3 font-bold lg:w-48  w-40  shadow-xl rounded-xl text-center absolute">
                  {t("gooldprice")}
                </div>
              </div>

              <div className="bg-gradient-to-r from-[#AE8625] via-[#F7EF8A] to-[#D2AC47]  w-full h-full p-4 overflow-scroll md:overflow-hidden">
                <div className="flex justify-between">
                  <div className="text-white">{t("gooldpricetable")}</div>
                  <div className="flex flex-col">
                    <div className="text-white">ประจำวันวันที่ {datetime}</div>
                    <div className="text-white text-end">
                      เวลา {formattedTime} น.
                    </div>
                  </div>
                </div>
                <div className="flex flex-col w-screen md:w-full overflow-x-auto md:overflow-x-hidden">
                  <div className="pt-8 pb-3">
                    <div className="bg-[#111827] ">
                      <div className="px-4 py-2 grid grid-cols-4 w-full">
                        <div className="text-white w-[10rem]">ประเภททอง</div>
                        <div className="text-red-400 w-[10rem]">รับซื้อ</div>
                        <div className="text-green-600 w-[10rem]">ขายออก</div>
                        <div className="text-blue-600 w-[10rem]">
                          เปลี่ยนแปลง
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="bg-[#a1a1aa] bg-opacity-40 w-full  h-full">
                    <div className="px-4 py-2 grid grid-cols-4 items-center overflow-scroll md:overflow-hidden">
                      <div className="flex flex-col gap-1  md:overflow-hidden">
                        <div className="text-white">ทองคำแท่ง</div>
                        <div className="text-white">96.5%</div>
                        <div className="text-yellow-600">สมาคม</div>
                      </div>
                      <div className="text-white lg:text-2xl xl:text-3xl">
                        32,300
                      </div>
                      <div className="text-white lg:text-2xl xl:text-3xl">
                        32,200
                      </div>
                      <div className="text-white lg:text-2xl xl:text-3xl">
                        50
                      </div>
                    </div>
                  </div>
                  <div className="bg-[#a1a1aa] bg-opacity-40 w-full  h-full">
                    <div className="px-4 py-2 grid grid-cols-4 items-center overflow-scroll md:overflow-hidden">
                      <div className="flex flex-col gap-1">
                        <div className="text-white">ทองคำแท่ง</div>
                        <div className="text-white">96.5%</div>
                        <div className="text-yellow-600">สมาคม</div>
                      </div>
                      <div className="text-white lg:text-2xl xl:text-3xl">
                        32,300
                      </div>
                      <div className="text-white lg:text-2xl xl:text-3xl">
                        32,200
                      </div>
                      <div className="text-white lg:text-2xl xl:text-3xl">
                        50
                      </div>
                    </div>
                  </div>
                  <div className="bg-[#a1a1aa] bg-opacity-40 w-full  h-full">
                    <div className="px-4 py-2 grid grid-cols-4 items-center overflow-scroll md:overflow-hidden">
                      <div className="flex flex-col gap-1">
                        <div className="text-white">ทองคำแท่ง</div>
                        <div className="text-white">96.5%</div>
                        <div className="text-yellow-600">สมาคม</div>
                      </div>
                      <div className="text-white lg:text-2xl xl:text-3xl">
                        32,300
                      </div>
                      <div className="text-white lg:text-2xl xl:text-3xl">
                        32,200
                      </div>
                      <div className="text-white lg:text-2xl xl:text-3xl">
                        50
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div
              className="bg-white border rounded-none w-full h-full p-2"
              data-aos="flip-left"
              data-aos-easing="ease-out-cubic"
              data-aos-duration="3000"
            >
              <div className="flex justify-center items-center">
                <div className=" text-base top-[-1.5rem] md:top-[-1.5rem] lg:-top-6 cursor-pointer text-white bg-gradient-to-r from-[#F7eF8A] to-[#D2AC47] lg:text-lg p-3 font-bold lg:w-48  w-40 h-f shadow-lg rounded-lg text-center absolute">
                  {t("gooldpricechart")}
                </div>
              </div>
              <div className=" bg-gradient-to-r from-[#AE8625] via-[#F7EF8A] to-[#D2AC47] w-full h-full p-4 overflow-scroll md:overflow-hidden">
                <div className="flex justify-between">
                  <div className="text-white md:text-4xl ">
                    {t("thepriceofgold")}
                  </div>
                </div>
                <div className="tradingview-widget-container">
                  <div id="tradingview_9033b" />
                  <div className="tradingview-widget-copyright"></div>
                </div>
                <div className="text-right text-white text-lg font-medium">
                  ราคาทองคำต่างประเทศโดย TradingView
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
